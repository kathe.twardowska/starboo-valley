﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour {

	public SceneField SceneToLoad;

	private void OnTriggerEnter2D(Collider2D collision) {
		if(collision.transform.tag == "Player") {
			SceneManager.LoadScene(SceneToLoad);
		}
	}
}
