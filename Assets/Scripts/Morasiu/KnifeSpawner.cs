﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeSpawner : MonoBehaviour {

	public int KnifeAmount = 5;
	public int DestroyAfter = 3;
	public float RandomSpawnRange = 0.5f;
	public Transform SpawnTrasform;

	[SerializeField]
	private GameObject KnifePrefab;
	List<GameObject> Knifes;

	void Start () {
		Knifes = new List<GameObject>();
		for (int i = 0; i < 5; i++) {
			var knife = Instantiate(KnifePrefab, transform);
			knife.SetActive(false);
			Knifes.Add(knife);
		}

		StartCoroutine(Spawn());
	}

	IEnumerator Spawn() {
		while(true) {
			for (int i = 0; i < Knifes.Count; i++) {
				Knifes[i].SetActive(false);
				Knifes[i].transform.position = Vector3.zero;
			}

			yield return new WaitForSeconds(0.5f);
			for (int i = 0; i < Knifes.Count; i++) {
				yield return new WaitForSeconds(Random.Range(0f, 0.4f));
				Knifes[i].SetActive(true);
				Knifes[i].transform.position = SpawnTrasform.position + new Vector3(Random.Range(-RandomSpawnRange, RandomSpawnRange),
																					Random.Range(-RandomSpawnRange, RandomSpawnRange), 0);
			}
			yield return new WaitForSeconds(3);
		}
	}
}
