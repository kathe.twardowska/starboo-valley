﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour {
	private const float RotationSpeed = 0.2f;
	public bool FacingRight = true;
	public Transform FlippingObject;
	private bool IsRotating;

	void Start () {
		StartCoroutine(CheckForRotation());
	}

	IEnumerator CheckForRotation() {
		var start = FlippingObject.rotation;
		while(true) {
			var firstPositionX = FlippingObject.position.x;
			yield return new WaitForSeconds(0.01f);
			if (FlippingObject.position.x > firstPositionX && !FacingRight) {
				var targetRotation = Quaternion.Euler(0, FlippingObject.rotation.eulerAngles.y + 180, 0);
				while (Mathf.Round(FlippingObject.rotation.eulerAngles.y) != Mathf.Round(targetRotation.eulerAngles.y)) {
					FlippingObject.rotation =  Quaternion.Lerp(FlippingObject.rotation, targetRotation, 0.1f);
					yield return new WaitForEndOfFrame();
				}

				FacingRight = true;
			} else if (FlippingObject.position.x < firstPositionX && FacingRight) {
				var targetRotation = Quaternion.Euler(0, FlippingObject.rotation.eulerAngles.y + 180, 0);
				while (Mathf.Round(FlippingObject.rotation.eulerAngles.y) != Mathf.Round(targetRotation.eulerAngles.y)) {
					FlippingObject.rotation = Quaternion.Lerp(FlippingObject.rotation, targetRotation, RotationSpeed);
					yield return new WaitForEndOfFrame();
				}
				FacingRight = false;

			}
		}
	}
}
