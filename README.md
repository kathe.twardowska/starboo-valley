# Duszek
Gierka o niepozornym duchu - Boo.

[![IMAGE ALT Boo!!](https://img.youtube.com/vi/P4t-80BrNfg/0.jpg)](https://www.youtube.com/watch?v=P4t-80BrNfg)

Wiele lat temy żył człowiek, który miał dom. Ten dom wiele dla niego znaczył. Nawet gdy zmarł i przemienił się w ducha, nadal strzegł swoich włości. Pewnego dnia, gdy na chwilę oddalił się od domu, wprowadziła się tam nowa rodzina... Ta rodzina jeszcze nie wie, że nikt nie będzie mieszkał w domu należącym do Boo.

👻~!
